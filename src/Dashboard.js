import React from 'react';
import './App.css';
import Cookies from 'js-cookie'

import { withRouter } from 'react-router-dom';

class Dashboard extends React.Component {

    componentDidMount() {


        document.addEventListener("message", function (e) {
            const ac = Cookies.get('danain_session')
            alert(e.data)
            if ((ac == null) && (e.data == null)) {
                this.props.history.push('/login')
                return;
            }
            Cookies.set('danain_session', e.data)
        });
    }

    handleCheckCookies = () => {

        const ac = Cookies.get('danain_session')
        alert(ac);

    }

    handleEraseLocalStorage = () => {
        Cookies.remove('danain_session')
        this.props.history.push('/login')
        window.ReactNativeWebView.postMessage("remove-ac")
    }

    render() {
        return (
            <div className="App">
                <h1 onClick={this.handleCheckCookies}>You are in, check it!</h1>
                <button onClick={this.handleEraseLocalStorage}>Hapus</button>
            </div>
        );

    }

}

export default withRouter(Dashboard);
