
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import App from './App';
import Dashboard from './Dashboard';

class Root extends React.Component {
    render() {
        return (
            <div>
                <Router>
                    <Switch>
                        <Route exact path="/login" component={App} />
                        <Route exact path="/" component={Dashboard} />
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default Root;